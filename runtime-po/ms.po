# Bison-runtime Bahasa Melayu (Malay) (ms).
# Copyright (C) 2003 Free Software Foundation, Inc.
# This file is distributed under the same license as the Bison-runtime package.
# Sharuzzaman Ahmat Raslan <sharuzzaman@myrealbox.com>, 2005.
#
msgid ""
msgstr ""
"Project-Id-Version: bison-runtime 2.1\n"
"Report-Msgid-Bugs-To: bug-bison@gnu.org\n"
"POT-Creation-Date: 2012-08-03 09:44+0200\n"
"PO-Revision-Date: 2005-10-10 10:50+0800\n"
"Last-Translator: Sharuzzaman Ahmat Raslan <sharuzzaman@myrealbox.com>\n"
"Language-Team: Malay <translation-team-ms@lists.sourceforge.net>\n"
"Language: ms\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: KBabel 0.9.5\n"

#: data/glr.c:882 data/yacc.c:739
msgid "syntax error: cannot back up"
msgstr "ralat sintaks: tidak dapat mengundur"

#: data/glr.c:1756
msgid "syntax is ambiguous"
msgstr "sintaks adalah kabur"

#: data/glr.c:2041 data/glr.c:2119 data/glr.c:2157 data/glr.c:2426
#: data/lalr1.cc:912 data/lalr1.cc:932 data/yacc.c:1336 data/yacc.c:1851
#: data/yacc.c:1857
msgid "syntax error"
msgstr "ralat sintaks"

#: data/glr.c:2120 data/lalr1.cc:913 data/yacc.c:1337
#, c-format
msgid "syntax error, unexpected %s"
msgstr "ralat sintaks, tidak menjangka %s"

#: data/glr.c:2121 data/lalr1.cc:914 data/yacc.c:1338
#, c-format
msgid "syntax error, unexpected %s, expecting %s"
msgstr "ralat sintaks, tidak menjangka %s, menjangka %s"

#: data/glr.c:2122 data/lalr1.cc:915 data/yacc.c:1339
#, c-format
msgid "syntax error, unexpected %s, expecting %s or %s"
msgstr "ralat sintaks, tidak menjangka %s, menjangka %s atau %s"

#: data/glr.c:2123 data/lalr1.cc:916 data/yacc.c:1340
#, c-format
msgid "syntax error, unexpected %s, expecting %s or %s or %s"
msgstr "ralat sintaks, tidak menjangka %s, menjangka %s atau %s atau %s"

#: data/glr.c:2124 data/lalr1.cc:917 data/yacc.c:1341
#, c-format
msgid "syntax error, unexpected %s, expecting %s or %s or %s or %s"
msgstr ""
"ralat sintaks, tidak menjangka %s, menjangka %s atau %s atau %s atau %s"

#: data/glr.c:2486 data/yacc.c:1418 data/yacc.c:1420 data/yacc.c:1613
#: data/yacc.c:2006
msgid "memory exhausted"
msgstr "kehabisan memori"
