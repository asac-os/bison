# Translation of bison-runtime messages to Russian
# Copyright (C) 2009 Free Software Foundation, Inc.
# This file is distributed under the same license as the bison package.
#
# Dmitry S. Sivachenko <dima@Chg.RU>, 1999,2000,2001,2002.
# Dimitriy Ryazantcev <DJm00n@mail.ru>, 2009.
# Pavel Maryanov <acid_jack@ukr.net>, 2009.
#
msgid ""
msgstr ""
"Project-Id-Version: bison-runtime 2.4\n"
"Report-Msgid-Bugs-To: bug-bison@gnu.org\n"
"POT-Creation-Date: 2012-08-03 09:44+0200\n"
"PO-Revision-Date: 2009-02-04 19:48+0200\n"
"Last-Translator: Pavel Maryanov <acid_jack@ukr.net>\n"
"Language-Team: Russian <gnu@mx.ru>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: data/glr.c:882 data/yacc.c:739
msgid "syntax error: cannot back up"
msgstr "синтаксическая ошибка: не удалось создать резервную копию"

#: data/glr.c:1756
msgid "syntax is ambiguous"
msgstr "неоднозначный синтаксис"

#: data/glr.c:2041 data/glr.c:2119 data/glr.c:2157 data/glr.c:2426
#: data/lalr1.cc:912 data/lalr1.cc:932 data/yacc.c:1336 data/yacc.c:1851
#: data/yacc.c:1857
msgid "syntax error"
msgstr "синтаксическая ошибка"

#: data/glr.c:2120 data/lalr1.cc:913 data/yacc.c:1337
#, c-format
msgid "syntax error, unexpected %s"
msgstr "синтаксическая ошибка, непредвиденная %s"

#: data/glr.c:2121 data/lalr1.cc:914 data/yacc.c:1338
#, c-format
msgid "syntax error, unexpected %s, expecting %s"
msgstr "синтаксическая ошибка, непредвиденная %s, ожидается %s"

#: data/glr.c:2122 data/lalr1.cc:915 data/yacc.c:1339
#, c-format
msgid "syntax error, unexpected %s, expecting %s or %s"
msgstr "синтаксическая ошибка, непредвиденная %s, ожидается %s или %s"

#: data/glr.c:2123 data/lalr1.cc:916 data/yacc.c:1340
#, c-format
msgid "syntax error, unexpected %s, expecting %s or %s or %s"
msgstr "синтаксическая ошибка, непредвиденная %s, ожидается %s или %s, или %s"

#: data/glr.c:2124 data/lalr1.cc:917 data/yacc.c:1341
#, c-format
msgid "syntax error, unexpected %s, expecting %s or %s or %s or %s"
msgstr ""
"синтаксическая ошибка, непредвиденная %s, ожидается %s или %s, или %s, или %s"

#: data/glr.c:2486 data/yacc.c:1418 data/yacc.c:1420 data/yacc.c:1613
#: data/yacc.c:2006
msgid "memory exhausted"
msgstr "недостаточно памяти"
