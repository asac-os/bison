# Ukrainian translation of bison.
# Copyright (C) 2007 Free Software Foundation, Inc.
# This file is distributed under the same license as the bison package.
#
# Maxim V. Dziumanenko <dziumanenko@gmail.com>, 2007.
# Yuri Chornoivan <yurchor@ukr.net>, 2012.
msgid ""
msgstr ""
"Project-Id-Version: bison 2.5.90\n"
"Report-Msgid-Bugs-To: bug-bison@gnu.org\n"
"POT-Creation-Date: 2012-08-03 09:44+0200\n"
"PO-Revision-Date: 2012-07-05 22:00+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <translation-team-uk@lists.sourceforge.net>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 1.5\n"

#: src/complain.c:116 src/complain.c:127 src/complain.c:136 src/complain.c:184
#: src/complain.c:194
msgid "warning"
msgstr "попередження"

#: src/complain.c:204 src/complain.c:211
msgid "fatal error"
msgstr "критична помилка: "

#: src/conflicts.c:77
#, c-format
msgid "    Conflict between rule %d and token %s resolved as shift"
msgstr "    Конфлікт між правилом %d та лексемою %s розв'язаний зсувом"

#: src/conflicts.c:86
#, c-format
msgid "    Conflict between rule %d and token %s resolved as reduce"
msgstr "    Конфлікт між правилом %d та лексемою %s розв'язаний виводу"

#: src/conflicts.c:94
#, c-format
msgid "    Conflict between rule %d and token %s resolved as an error"
msgstr "    Конфлікт між правилом %d та лексемою %s розв'язаний як помилка"

#: src/conflicts.c:492
#, c-format
msgid "conflicts: %d shift/reduce, %d reduce/reduce\n"
msgstr "конфлікти: %d зсуву/виводу, %d виводу/виводу\n"

#: src/conflicts.c:495
#, c-format
msgid "conflicts: %d shift/reduce\n"
msgstr "конфлікти: %d зсуву/виводу\n"

#: src/conflicts.c:497
#, c-format
msgid "conflicts: %d reduce/reduce\n"
msgstr "конфлікти: %d виводу/виводу\n"

#: src/conflicts.c:515
#, c-format
msgid "State %d "
msgstr "Стан %d "

#: src/conflicts.c:582
#, c-format
msgid "%%expect-rr applies only to GLR parsers"
msgstr "%%expect-rr застосовується лише до аналізатору GLR"

#: src/conflicts.c:616
#, c-format
msgid "expected %d shift/reduce conflict"
msgid_plural "expected %d shift/reduce conflicts"
msgstr[0] "очікувався %d конфлікт зсуву/виводу"
msgstr[1] "очікувалось %d конфлікти зсуву/виводу"
msgstr[2] "очікувалось %d конфліктів зсуву/виводу"

#: src/conflicts.c:621
#, c-format
msgid "expected %d reduce/reduce conflict"
msgid_plural "expected %d reduce/reduce conflicts"
msgstr[0] "очікувалось %d конфлікт виводу/виводу"
msgstr[1] "очікувалось %d конфлікти виводу/виводу"
msgstr[2] "очікувалось %d конфліктів виводу/виводу"

#: src/files.c:114
#, c-format
msgid "%s: cannot open"
msgstr "%s: не вдалося відкрити"

#: src/files.c:130
#, c-format
msgid "input/output error"
msgstr "помилка введення-виведення"

#: src/files.c:133
#, c-format
msgid "cannot close file"
msgstr "не вдається закрити файл"

#: src/files.c:352
#, c-format
msgid "refusing to overwrite the input file %s"
msgstr "відмовлено у перезаписі файла вхідних даних %s"

#: src/files.c:362
#, c-format
msgid "conflicting outputs to file %s"
msgstr "суперечливий вивід у файл %s"

#: src/getargs.c:271
#, c-format
msgid "Try `%s --help' for more information.\n"
msgstr "Використовуйте `%s --help' для додаткової інформації.\n"

#: src/getargs.c:280
#, c-format
msgid "Usage: %s [OPTION]... FILE\n"
msgstr "Використання: %s [КЛЮЧІ]... ФАЙЛ\n"

#: src/getargs.c:281
msgid ""
"Generate a deterministic LR or generalized LR (GLR) parser employing\n"
"LALR(1), IELR(1), or canonical LR(1) parser tables.  IELR(1) and\n"
"canonical LR(1) support is experimental.\n"
"\n"
msgstr ""
"Створити детерміністичний аналізатор LR або узагальнений LR (GLR) з\n"
"використанням LALR(1), IELR(1) або канонічних таблиць LR(1). Підтримку\n"
"IELR(1) та канонічних таблиць LR(1) ще недостатньо перевірено.\n"
"\n"

#: src/getargs.c:288
msgid ""
"Mandatory arguments to long options are mandatory for short options too.\n"
msgstr ""
"Обов’язкові аргументи для довгих форм запису параметрів є обов’язковими і "
"для скорочених форм.\n"

#: src/getargs.c:291
msgid "The same is true for optional arguments.\n"
msgstr "Те саме стосується необов’язкових аргументів.\n"

#: src/getargs.c:295
msgid ""
"\n"
"Operation modes:\n"
"  -h, --help                 display this help and exit\n"
"  -V, --version              output version information and exit\n"
"      --print-localedir      output directory containing locale-dependent "
"data\n"
"      --print-datadir        output directory containing skeletons and XSLT\n"
"  -y, --yacc                 emulate POSIX Yacc\n"
"  -W, --warnings[=CATEGORY]  report the warnings falling in CATEGORY\n"
"\n"
msgstr ""
"\n"
"Режими роботи:\n"
"  -h, --help                 вивести цю довідку і завершити роботу\n"
"  -V, --version              вивести інформацію щодо версії і завершити "
"роботу\n"
"      --print-localedir      вивести назву каталогу, де містяться залежні "
"від локалі дані\n"
"      --print-datadir        вивести назву каталогу, де містяться шаблони та "
"XSLT\n"
"  -y, --yacc                 імітувати Yacc POSIX\n"
"  -W, --warnings[=КАТЕГОРІЯ] виводити попередження з категорії КАТЕГОРІЯ\n"
"\n"

#: src/getargs.c:307
#, c-format
msgid ""
"Parser:\n"
"  -L, --language=LANGUAGE          specify the output programming language\n"
"                                   (this is an experimental feature)\n"
"  -S, --skeleton=FILE              specify the skeleton to use\n"
"  -t, --debug                      instrument the parser for debugging\n"
"      --locations                  enable location support\n"
"  -D, --define=NAME[=VALUE]        similar to '%define NAME \"VALUE\"'\n"
"  -F, --force-define=NAME[=VALUE]  override '%define NAME \"VALUE\"'\n"
"  -p, --name-prefix=PREFIX         prepend PREFIX to the external symbols\n"
"                                   deprecated by '-Dapi.prefix=PREFIX'\n"
"  -l, --no-lines                   don't generate '#line' directives\n"
"  -k, --token-table                include a table of token names\n"
"\n"
msgstr ""
"Аналізатор:\n"
"  -L, --language=МОВА              вказати мову програмування виведених "
"даних\n"
"                                   (недостатньо перевірена можливість)\n"
"  -S, --skeleton=ФАЙЛ              вказати шаблон, який слід використати\n"
"  -t, --debug                      налаштувати аналізатор для діагностики\n"
"      --locations                  увімкнути підтримку місць\n"
"  -D, --define=НАЗВА[=ЗНАЧЕННЯ]    відповідник `%define НАЗВА \"ЗНАЧЕННЯ\"'\n"
"  -F, --force-define=НАЗВА[=ЗНАЧЕННЯ] перевизначити `%define НАЗВА \"ЗНАЧЕННЯ"
"\"'\n"
"  -p, --name-prefix=ПРЕФІКС        додавати ПРЕФІКС до зовнішніх символів,\n"
"                                   слід надавати перевагу '-Dapi."
"prefix=ПРЕФІКС'\n"
"  -l, --no-lines                   не створювати інструкцій `#line'\n"
"  -k, --token-table                включити таблицю назв елементів\n"
"\n"

#: src/getargs.c:325
msgid ""
"Output:\n"
"      --defines[=FILE]       also produce a header file\n"
"  -d                         likewise but cannot specify FILE (for POSIX "
"Yacc)\n"
"  -r, --report=THINGS        also produce details on the automaton\n"
"      --report-file=FILE     write report to FILE\n"
"  -v, --verbose              same as `--report=state'\n"
"  -b, --file-prefix=PREFIX   specify a PREFIX for output files\n"
"  -o, --output=FILE          leave output to FILE\n"
"  -g, --graph[=FILE]         also output a graph of the automaton\n"
"  -x, --xml[=FILE]           also output an XML report of the automaton\n"
"                             (the XML schema is experimental)\n"
"\n"
msgstr ""
"Виведення:\n"
"      --defines[=ФАЙЛ]       створити також файл заголовків\n"
"  -d                         подібне, але без можливості вказати ФАЙЛ (для "
"Yacc POSIX)\n"
"  -r, --report=ЕЛЕМЕНТИ      також створити автоматичні записи подробиць\n"
"      --report-file=ФАЙЛ     записати звіт до ФАЙЛа\n"
"  -v, --verbose              те саме, що і `--report=state'\n"
"  -b, --file-prefix=ПРЕФІКС  вказати ПРЕФІКС файла результатів\n"
"  -o, --output=ФАЙЛ          вивести дані до ФАЙЛа\n"
"  -g, --graph[=ФАЙЛ]         вивести граф автомата\n"
"  -x, --xml[=ФАЙЛ]           вивести звіт автомата у форматі XML\n"
"                             (схему XML перевірено недостатньо)\n"
"\n"

#: src/getargs.c:340
msgid ""
"Warning categories include:\n"
"  `midrule-values'  unset or unused midrule values\n"
"  `yacc'            incompatibilities with POSIX Yacc\n"
"  `conflicts-sr'    S/R conflicts (enabled by default)\n"
"  `conflicts-rr'    R/R conflicts (enabled by default)\n"
"  `other'           all other warnings (enabled by default)\n"
"  `all'             all the warnings\n"
"  `no-CATEGORY'     turn off warnings in CATEGORY\n"
"  `none'            turn off all the warnings\n"
"  `error'           treat warnings as errors\n"
"\n"
msgstr ""
"Категорії попереджень:\n"
"  `midrule-values'  невстановлені або невикористані проміжні значення\n"
"  `yacc'            несумісності з Yacc POSIX\n"
"  `conflicts-sr'    S/R-конфлікти (типово увімкнено)\n"
"  `conflicts-rr'    R/R conflicts (типово увімкнено)\n"
"  `other'           всі інші попередження (типово увімкнено)\n"
"  `all'             всі попередження\n"
"  `no-КАТЕГОРІЯ'    вимкнути попередження з КАТЕГОРІЇ\n"
"  `none'            вимкнути всі попередження\n"
"  `error'           вважати попередження помилками\n"
"\n"

#: src/getargs.c:354
msgid ""
"THINGS is a list of comma separated words that can include:\n"
"  `state'        describe the states\n"
"  `itemset'      complete the core item sets with their closure\n"
"  `lookahead'    explicitly associate lookahead tokens to items\n"
"  `solved'       describe shift/reduce conflicts solving\n"
"  `all'          include all the above information\n"
"  `none'         disable the report\n"
msgstr ""
"THINGS - список розділених комою слів, які можуть включати:\n"
"  `state'        описує стани\n"
"  `itemset'      завершити набір елементів ядра та закрити їх\n"
"  `lookahead'    явна прив'язати ознаки lookahead до елементів\n"
"  `solved'       описати вирішення конфліктів зсуву/виводу\n"
"  `all'          включати всю наведену вище інформацію\n"
"  `none'         вимкнути звіт\n"

#: src/getargs.c:364
#, c-format
msgid ""
"\n"
"Report bugs to <%s>.\n"
msgstr ""
"\n"
"Сповіщайте про помилки за адресою <%s>.\n"

#: src/getargs.c:380
#, c-format
msgid "bison (GNU Bison) %s"
msgstr "bison (GNU Bison) %s"

#: src/getargs.c:382
msgid "Written by Robert Corbett and Richard Stallman.\n"
msgstr "Автори: Роберт Корбет та Річард Столмен.\n"

#: src/getargs.c:386
#, c-format
msgid "Copyright (C) %d Free Software Foundation, Inc.\n"
msgstr "Copyright (C) %d Free Software Foundation, Inc.\n"

#: src/getargs.c:389
msgid ""
"This is free software; see the source for copying conditions.  There is NO\n"
"warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.\n"
msgstr ""
"Це програмне забезпечення є вільним, умови копіювання викладено у його "
"початкових кодах.\n"
"Умовами ліцензування програми НЕ передбачено жодних гарантій, зокрема "
"гарантій працездатності\n"
"або придатності для певної мети.\n"

#: src/getargs.c:410
#, c-format
msgid "multiple skeleton declarations are invalid"
msgstr "не можна використовувати декілька оголошень шаблонів"

#: src/getargs.c:428
#, c-format
msgid "%s: invalid language"
msgstr "%s: некоректна мова"

#: src/getargs.c:431
msgid "multiple language declarations are invalid"
msgstr "визначення декількох оголошень мов є некоректним"

#: src/getargs.c:693
#, c-format
msgid "%s: missing operand"
msgstr "%s: пропущено операнд"

#: src/getargs.c:695
#, c-format
msgid "extra operand %s"
msgstr "зайвий операнд %s"

#: src/gram.c:112
msgid "empty"
msgstr "порожній"

#: src/gram.c:201
msgid "Grammar"
msgstr "Граматика"

#: src/graphviz.c:43
#, c-format
msgid ""
"// Generated by %s.\n"
"// Report bugs to <%s>.\n"
"// Home page: <%s>.\n"
"\n"
msgstr ""
"// Створено за допомогою %s.\n"
"// Про вади повідомляйте на адресу <%s>.\n"
"// Домашня сторінка: <%s>.\n"
"\n"

#: src/location.c:93 src/scan-gram.l:855
#, c-format
msgid "line number overflow"
msgstr "переповнення номеру рядка"

#: src/location.c:95
#, c-format
msgid "column number overflow"
msgstr "переповнення номеру стовпчика"

#: src/main.c:146
msgid "rule useless in parser due to conflicts"
msgstr "правило є зайвим у аналізаторі через конфлікти"

#: src/muscle-tab.c:436
#, c-format
msgid "%%define variable %s redefined"
msgstr "змінну %%define %s перевизначено"

#: src/muscle-tab.c:439
#, c-format
msgid "previous definition"
msgstr "попереднє визначення"

#: src/muscle-tab.c:478 src/muscle-tab.c:492 src/muscle-tab.c:544
#: src/muscle-tab.c:609
#, c-format
msgid "%s: undefined %%define variable %s"
msgstr "%s: невизначена змінна %%define %s"

#: src/muscle-tab.c:538
#, c-format
msgid "invalid value for %%define Boolean variable %s"
msgstr "некоректне значення булевої змінної %%define %s"

#: src/muscle-tab.c:596
#, c-format
msgid "invalid value for %%define variable %s: %s"
msgstr "некоректне значення змінної %%define %s: %s"

#: src/muscle-tab.c:599
#, c-format
msgid "accepted value: %s"
msgstr "прийняте значення: %s"

#: src/parse-gram.y:746
#, c-format
msgid "missing identifier in parameter declaration"
msgstr "у описі параметра відсутній ідентифікатор"

#: src/print.c:47
#, c-format
msgid " type %d is %s\n"
msgstr " тип %d є %s\n"

#: src/print.c:164
#, c-format
msgid "shift, and go to state %d\n"
msgstr "зсув, та перехід у стан %d\n"

#: src/print.c:166
#, c-format
msgid "go to state %d\n"
msgstr "перехід у стан %d\n"

#: src/print.c:203
msgid "error (nonassociative)\n"
msgstr "помилка (не асоціативна)\n"

#: src/print.c:226
#, c-format
msgid "reduce using rule %d (%s)"
msgstr "вивід з використанням правила %d (%s)"

#: src/print.c:228
#, c-format
msgid "accept"
msgstr "прийняти"

#: src/print.c:264 src/print.c:338
msgid "$default"
msgstr "$default"

#: src/print.c:373
#, c-format
msgid "state %d"
msgstr "стан %d"

#: src/print.c:409
msgid "Terminals, with rules where they appear"
msgstr "Термінальні символи з правилами, у яких вони з'являються"

#: src/print.c:436
msgid "Nonterminals, with rules where they appear"
msgstr "Нетермінальні символи з правилами, у яких вони з'являються"

#: src/print.c:465
#, c-format
msgid " on left:"
msgstr " ліворуч:"

#: src/print.c:482
#, c-format
msgid " on right:"
msgstr " праворуч:"

#: src/print.c:510
msgid "Rules useless in parser due to conflicts"
msgstr "Правила є зайвими у аналізаторі через конфлікти"

#: src/reader.c:62
#, c-format
msgid "multiple %s declarations"
msgstr "багатократні описи %s"

#: src/reader.c:132
#, c-format
msgid "result type clash on merge function %s: <%s> != <%s>"
msgstr "конфлікт типів результатів під час об’єднання функції %s: <%s> != <%s>"

#: src/reader.c:135 src/symtab.c:154 src/symtab.c:162 src/symtab.c:929
#: src/symtab.c:942 src/symtab.c:955 src/symtab.c:968
#, c-format
msgid "previous declaration"
msgstr "попереднє оголошення"

#: src/reader.c:201
#, c-format
msgid "duplicated symbol name for %s ignored"
msgstr "проігноровано дублікат назви символу %s"

#: src/reader.c:245
#, c-format
msgid "rule given for %s, which is a token"
msgstr "правило задано для %s, який є лексемою"

#: src/reader.c:300
#, c-format
msgid "type clash on default action: <%s> != <%s>"
msgstr "конфлікт типів на типовій дії: <%s> != <%s>"

#: src/reader.c:306
#, c-format
msgid "empty rule for typed nonterminal, and no action"
msgstr ""
"порожнє правило для типізованого нетермінального символу, та відсутня дія"

#: src/reader.c:324
#, c-format
msgid "unused value: $%d"
msgstr "не використане значення: $%d"

#: src/reader.c:326
msgid "unset value: $$"
msgstr "не встановлене значення: $$"

#: src/reader.c:337
#, c-format
msgid "token for %%prec is not defined: %s"
msgstr "елемент %%prec не визначено: %s"

#: src/reader.c:427 src/reader.c:441 src/reader.c:454
#, c-format
msgid "only one %s allowed per rule"
msgstr "у правилі допускається лише один %s"

#: src/reader.c:437 src/reader.c:452
#, c-format
msgid "%s affects only GLR parsers"
msgstr "%s впливає лише на аналізатори GLR"

#: src/reader.c:439
#, c-format
msgid "%s must be followed by positive number"
msgstr "за %s повинно слідувати додатне число"

#: src/reader.c:550
#, c-format
msgid "rule is too long"
msgstr "правило надто довге"

#: src/reader.c:668
#, c-format
msgid "no rules in the input grammar"
msgstr "відсутні правила у вхідній граматиці"

#: src/reduce.c:241
msgid "rule useless in grammar"
msgstr "зайве правило у граматиці"

#: src/reduce.c:302
#, c-format
msgid "nonterminal useless in grammar: %s"
msgstr "нетермінал є зайвим у граматиці: %s"

#: src/reduce.c:350
msgid "Nonterminals useless in grammar"
msgstr "Нетермінали зайві у граматиці"

#: src/reduce.c:363
msgid "Terminals unused in grammar"
msgstr "Невикористані термінали у граматиці"

#: src/reduce.c:372
msgid "Rules useless in grammar"
msgstr "Зайві правила у граматиці"

#: src/reduce.c:385
#, c-format
msgid "%d nonterminal useless in grammar"
msgid_plural "%d nonterminals useless in grammar"
msgstr[0] "%d зайвий нетермінал у граматиці"
msgstr[1] "%d зайвих нетермінали у граматиці"
msgstr[2] "%d зайвих нетерміналів у граматиці"

#: src/reduce.c:390
#, c-format
msgid "%d rule useless in grammar"
msgid_plural "%d rules useless in grammar"
msgstr[0] "%d зайве правило у граматиці"
msgstr[1] "%d зайвих правила у граматиці"
msgstr[2] "%d зайвих правил у граматиці"

#: src/reduce.c:419
#, c-format
msgid "start symbol %s does not derive any sentence"
msgstr "початковий символ %s не виводить жодного речення"

#: src/scan-code.l:188
#, fuzzy, c-format
msgid "stray '%s'"
msgstr "зайвий символ '$'"

#: src/scan-code.l:227
#, c-format
msgid "a ';' might be needed at the end of action code"
msgstr "наприкінці коду дії можливо потрібен символ ';'"

#: src/scan-code.l:228
#, c-format
msgid "future versions of Bison will not add the ';'"
msgstr "у майбутніх версіях Bison символ ';' не додаватиметься"

#: src/scan-code.l:250
#, c-format
msgid "use of YYFAIL, which is deprecated and will be removed"
msgstr ""
"використання YYFAIL. Вважається застарілим, буде вилучено у наступних "
"версіях."

#: src/scan-code.l:427 src/scan-code.l:430
#, c-format
msgid "refers to: %c%s at %s"
msgstr "посилається на: %c%s у %s"

#: src/scan-code.l:446
#, c-format
msgid "possibly meant: %c"
msgstr "ймовірно мало бути: %c"

#: src/scan-code.l:455
#, c-format
msgid ", hiding %c"
msgstr ", приховування %c"

#: src/scan-code.l:463
#, c-format
msgid " at %s"
msgstr " у %s"

#: src/scan-code.l:468
#, c-format
msgid ", cannot be accessed from mid-rule action at $%d"
msgstr ", доступ з дії проміжного правила неможливий у $%d"

#: src/scan-code.l:520 src/scan-gram.l:777
#, c-format
msgid "integer out of range: %s"
msgstr "вихід за межі діапазону цілого числа: %s"

#: src/scan-code.l:609
#, c-format
msgid "invalid reference: %s"
msgstr "некоректне посилання: %s"

#: src/scan-code.l:618
#, c-format
msgid "syntax error after '%c', expecting integer, letter, '_', '[', or '$'"
msgstr ""
"синтаксична помилка після '%c'. Мало бути вказано ціле число, літеру, '_', "
"'[' або '$'"

#: src/scan-code.l:625
#, c-format
msgid "symbol not found in production before $%d: %.*s"
msgstr "у продукції не знайдено символу до $%d: %.*s"

#: src/scan-code.l:632
#, c-format
msgid "symbol not found in production: %.*s"
msgstr "у продукції не знайдено символу: %.*s"

#: src/scan-code.l:647
#, c-format
msgid "misleading reference: %s"
msgstr "помилкове посилання: %s"

#: src/scan-code.l:662
#, c-format
msgid "ambiguous reference: %s"
msgstr "неоднозначне посилання: %s"

#: src/scan-code.l:699
#, c-format
msgid "explicit type given in untyped grammar"
msgstr "у нетипізованій граматиці явним чином вказано тип"

#: src/scan-code.l:758
#, c-format
msgid "$$ for the midrule at $%d of %s has no declared type"
msgstr "$$ для проміжного правила у $%d %s немає оголошеного типу"

#: src/scan-code.l:763
#, c-format
msgid "$$ of %s has no declared type"
msgstr "$$ %s не має оголошеного типу"

#: src/scan-code.l:785
#, c-format
msgid "$%s of %s has no declared type"
msgstr "$%s %s не має оголошеного типу"

#: src/scan-gram.l:149
#, c-format
msgid "stray ',' treated as white space"
msgstr "зайва кома, вважатиметься пробілом"

#: src/scan-gram.l:222
#, c-format
msgid "invalid directive: %s"
msgstr "некоректна директива: %s"

#: src/scan-gram.l:250
#, c-format
msgid "invalid identifier: %s"
msgstr "некоректний ідентифікатор: %s"

#: src/scan-gram.l:294
#, c-format
msgid "invalid character: %s"
msgstr "неприпустимий символ: %s"

#: src/scan-gram.l:352
#, c-format
msgid "unexpected identifier in bracketed name: %s"
msgstr "неочікуваний ідентифікатор у назву у дужках: %s"

#: src/scan-gram.l:374
#, c-format
msgid "an identifier expected"
msgstr "мало бути вказано ідентифікатор"

#: src/scan-gram.l:377
#, c-format
msgid "invalid character in bracketed name: %s"
msgstr "некоректний символ у назві у дужках: %s"

#: src/scan-gram.l:475 src/scan-gram.l:496
#, c-format
msgid "empty character literal"
msgstr "порожня символьна стала"

#: src/scan-gram.l:480 src/scan-gram.l:501
#, c-format
msgid "extra characters in character literal"
msgstr "зайві символи у символьній сталій"

#: src/scan-gram.l:512
#, c-format
msgid "invalid null character"
msgstr "неприпустимий null-символ"

#: src/scan-gram.l:525 src/scan-gram.l:535 src/scan-gram.l:555
#, c-format
msgid "invalid number after \\-escape: %s"
msgstr "некоректне число після екранування \\: %s"

#: src/scan-gram.l:567
#, c-format
msgid "invalid character after \\-escape: %s"
msgstr "некоректний символ після екранування \\: %s"

#: src/scan-gram.l:891
#, c-format
msgid "missing %s at end of file"
msgstr "не вистачає %s наприкінці файла"

#: src/scan-gram.l:902
#, c-format
msgid "missing %s at end of line"
msgstr "не вистачає %s наприкінці рядка"

#: src/scan-skel.l:146
#, c-format
msgid "unclosed %s directive in skeleton"
msgstr "незавершена інструкція %s у шаблоні"

#: src/scan-skel.l:291
#, c-format
msgid "too few arguments for %s directive in skeleton"
msgstr "занадто мало аргументів у інструкції %s шаблона"

#: src/scan-skel.l:298
#, c-format
msgid "too many arguments for %s directive in skeleton"
msgstr "занадто багато аргументів у інструкції %s шаблона"

#: src/symlist.c:211
#, c-format
msgid "invalid $ value: $%d"
msgstr "некоректне $ значення: $%d"

#: src/symtab.c:71
#, c-format
msgid "POSIX Yacc forbids dashes in symbol names: %s"
msgstr "Yacc POSIX забороняє використання дефісів у назва символів: %s"

#: src/symtab.c:91
#, c-format
msgid "too many symbols in input grammar (limit is %d)"
msgstr "надто багато символів (лексеми плюс нетермінали); максимально %d"

#: src/symtab.c:153
#, c-format
msgid "%s redeclaration for %s"
msgstr "повторний опис %s для %s"

#: src/symtab.c:161
#, c-format
msgid "%s redeclaration for <%s>"
msgstr "повторний опис %s для <%s>"

#: src/symtab.c:328
#, c-format
msgid "symbol %s redefined"
msgstr "повторне визначення символу %s"

#: src/symtab.c:342
#, c-format
msgid "symbol %s redeclared"
msgstr "символ %s визначений повторно"

#: src/symtab.c:363
#, c-format
msgid "redefining user token number of %s"
msgstr "перевизначення номера лексеми користувача для %s"

#: src/symtab.c:391
#, c-format
msgid "symbol %s is used, but is not defined as a token and has no rules"
msgstr ""
"символ %s використовується, але не визначений як лексема та не має правил"

#: src/symtab.c:411
#, c-format
msgid "symbol %s used more than once as a literal string"
msgstr "символ %s використано декілька разів як сталий рядок символів"

#: src/symtab.c:414
#, c-format
msgid "symbol %s given more than one literal string"
msgstr "символ %s вказано декілька сталих рядків (літералів)"

#: src/symtab.c:530
#, c-format
msgid "user token number %d redeclaration for %s"
msgstr "повторне оголошення елемента користувача з номером %d для %s"

#: src/symtab.c:532
#, c-format
msgid "previous declaration for %s"
msgstr "попереднє оголошення %s"

#: src/symtab.c:908
#, c-format
msgid "the start symbol %s is undefined"
msgstr "початковий символ %s невизначений"

#: src/symtab.c:912
#, c-format
msgid "the start symbol %s is a token"
msgstr "початковий символ %s є лексемою"

#: src/symtab.c:927
#, c-format
msgid "redeclaration for default tagged %%destructor"
msgstr "повторне оголошення типового %%destructor з міткою"

#: src/symtab.c:940
#, c-format
msgid "redeclaration for default tagless %%destructor"
msgstr "повторне оголошення типового %%destructor без мітки"

#: src/symtab.c:953
#, c-format
msgid "redeclaration for default tagged %%printer"
msgstr "повторне оголошення типового %%printer з міткою"

#: src/symtab.c:966
#, c-format
msgid "redeclaration for default tagless %%printer"
msgstr "повторне оголошення типового %%printer без мітки"

#: djgpp/subpipe.c:63 djgpp/subpipe.c:286 djgpp/subpipe.c:288
#, c-format
msgid "removing of '%s' failed"
msgstr "спроба вилучення «%s» зазнала невдачі"

#: djgpp/subpipe.c:85 djgpp/subpipe.c:92
#, c-format
msgid "creation of a temporary file failed"
msgstr "спроба створення тимчасового файла зазнала невдачі"

#: djgpp/subpipe.c:127
#, c-format
msgid "saving stdin failed"
msgstr "спроба збереження stdin зазнала невдачі"

#: djgpp/subpipe.c:131
#, c-format
msgid "saving stdout failed"
msgstr "спроба збереження stdout зазнала невдачі"

#: djgpp/subpipe.c:153 djgpp/subpipe.c:197 djgpp/subpipe.c:258
#, c-format
msgid "opening of tmpfile failed"
msgstr "спроба відкриття файла тимчасових даних зазнала невдачі"

#: djgpp/subpipe.c:157
#, c-format
msgid "redirecting bison's stdout to the temporary file failed"
msgstr ""
"спроба переспрямовування стандартного виведення bison до тимчасового файла "
"зазнала невдачі"

#: djgpp/subpipe.c:201
#, c-format
msgid "redirecting m4's stdin from the temporary file failed"
msgstr ""
"спроба переспрямовування стандартного джерела даних m4 з тимчасового файла "
"зазнала невдачі"

#: djgpp/subpipe.c:212
#, c-format
msgid "opening of a temporary file failed"
msgstr "не вдалося відкрити файл тимчасових даних"

#: djgpp/subpipe.c:218
#, c-format
msgid "redirecting m4's stdout to a temporary file failed"
msgstr ""
"спроба переспрямовування стандартного виведення m4 до тимчасового файла "
"зазнала невдачі"

#: djgpp/subpipe.c:234
#, c-format
msgid "subsidiary program '%s' interrupted"
msgstr "виконання допоміжної програми '%s' перервано"

#: djgpp/subpipe.c:241
#, c-format
msgid "subsidiary program '%s' not found"
msgstr "не вдалося знайти допоміжну програму '%s'"

#: djgpp/subpipe.c:265
#, c-format
msgid "redirecting bison's stdin from the temporary file failed"
msgstr ""
"спроба переспрямовування стандартного джерела даних bison з тимчасового "
"файла зазнала невдачі"

#: lib/argmatch.c:133
#, c-format
msgid "invalid argument %s for %s"
msgstr "неприпустимий аргумент %s для %s"

#: lib/argmatch.c:134
#, c-format
msgid "ambiguous argument %s for %s"
msgstr "неоднозначний аргумент %s для %s"

#: lib/argmatch.c:153
msgid "Valid arguments are:"
msgstr "Допустимі аргументи:"

#: lib/bitset_stats.c:177
#, c-format
msgid "%u bitset_allocs, %u freed (%.2f%%).\n"
msgstr "%u bitset_allocs, %u звільнено (%.2f%%).\n"

#: lib/bitset_stats.c:180
#, c-format
msgid "%u bitset_sets, %u cached (%.2f%%)\n"
msgstr "%u bitset_sets, %u кешовано (%.2f%%)\n"

#: lib/bitset_stats.c:183
#, c-format
msgid "%u bitset_resets, %u cached (%.2f%%)\n"
msgstr "%u bitset_resets, %u кешовано (%.2f%%)\n"

#: lib/bitset_stats.c:186
#, c-format
msgid "%u bitset_tests, %u cached (%.2f%%)\n"
msgstr "%u bitset_tests, %u кешовано (%.2f%%)\n"

#: lib/bitset_stats.c:190
#, c-format
msgid "%u bitset_lists\n"
msgstr "%u bitset_lists\n"

#: lib/bitset_stats.c:192
msgid "count log histogram\n"
msgstr "гістограма журналу лічильників\n"

#: lib/bitset_stats.c:195
msgid "size log histogram\n"
msgstr "гістограма журналу розміру\n"

#: lib/bitset_stats.c:198
msgid "density histogram\n"
msgstr "гістограма щільності\n"

#: lib/bitset_stats.c:212
#, c-format
msgid ""
"Bitset statistics:\n"
"\n"
msgstr ""
"Статистика бітових полів:\n"
"\n"

#: lib/bitset_stats.c:215
#, c-format
msgid "Accumulated runs = %u\n"
msgstr "Акумульовані запуски = %u\n"

#: lib/bitset_stats.c:259 lib/bitset_stats.c:264
msgid "cannot read stats file"
msgstr "не вдалося прочитати файл статистичних даних"

#: lib/bitset_stats.c:261
#, c-format
msgid "bad stats file size\n"
msgstr "помилковий розмір файла статистичних даних\n"

#: lib/bitset_stats.c:287 lib/bitset_stats.c:289
msgid "cannot write stats file"
msgstr "не вдалося записати файл статистичних даних."

#: lib/bitset_stats.c:292
msgid "cannot open stats file for writing"
msgstr "не вдалося відкрити файл статистичних даних для запису"

#: lib/closeout.c:112
msgid "write error"
msgstr "помилка запису"

#: lib/error.c:188
msgid "Unknown system error"
msgstr "Невідома системна помилка"

#: lib/getopt.c:547 lib/getopt.c:576
#, c-format
msgid "%s: option '%s' is ambiguous; possibilities:"
msgstr "%s: неоднозначний параметр «%s»; можливі варіанти:"

#: lib/getopt.c:624 lib/getopt.c:628
#, c-format
msgid "%s: option '--%s' doesn't allow an argument\n"
msgstr "%s: додавання аргументів до параметра «--%s» не передбачено\n"

#: lib/getopt.c:637 lib/getopt.c:642
#, c-format
msgid "%s: option '%c%s' doesn't allow an argument\n"
msgstr "%s: додавання аргументів до параметра «%c%s» не передбачено\n"

#: lib/getopt.c:685 lib/getopt.c:704
#, c-format
msgid "%s: option '--%s' requires an argument\n"
msgstr "%s: до параметра «--%s» слід додати аргумент\n"

#: lib/getopt.c:742 lib/getopt.c:745
#, c-format
msgid "%s: unrecognized option '--%s'\n"
msgstr "%s: невідомий параметр «--%s»\n"

#: lib/getopt.c:753 lib/getopt.c:756
#, c-format
msgid "%s: unrecognized option '%c%s'\n"
msgstr "%s: невідомий параметр «%c%s»\n"

#: lib/getopt.c:805 lib/getopt.c:808
#, c-format
msgid "%s: invalid option -- '%c'\n"
msgstr "%s: некоректний параметр — «%c»\n"

#: lib/getopt.c:861 lib/getopt.c:878 lib/getopt.c:1088 lib/getopt.c:1106
#, c-format
msgid "%s: option requires an argument -- '%c'\n"
msgstr "%s: до параметра слід додати аргумент — «%c»\n"

#: lib/getopt.c:934 lib/getopt.c:950
#, c-format
msgid "%s: option '-W %s' is ambiguous\n"
msgstr "%s: параметр «-W %s» не є однозначним\n"

#: lib/getopt.c:974 lib/getopt.c:992
#, c-format
msgid "%s: option '-W %s' doesn't allow an argument\n"
msgstr "%s: додавання аргументів до параметра «-W %s» не передбачено\n"

#: lib/getopt.c:1013 lib/getopt.c:1031
#, c-format
msgid "%s: option '-W %s' requires an argument\n"
msgstr "%s: до параметра «-W %s» слід додати аргумент\n"

#: lib/obstack.c:413 lib/obstack.c:415 lib/xalloc-die.c:34
msgid "memory exhausted"
msgstr "пам'ять вичерпано"

#: lib/spawn-pipe.c:138 lib/spawn-pipe.c:141 lib/spawn-pipe.c:262
#: lib/spawn-pipe.c:265
#, c-format
msgid "cannot create pipe"
msgstr "не вдалося створити канал"

#: lib/spawn-pipe.c:232 lib/spawn-pipe.c:346 lib/wait-process.c:282
#: lib/wait-process.c:356
#, c-format
msgid "%s subprocess failed"
msgstr "Помилка підпроцесу %s"

#. TRANSLATORS:
#. Get translations for open and closing quotation marks.
#. The message catalog should translate "`" to a left
#. quotation mark suitable for the locale, and similarly for
#. "'".  For example, a French Unicode local should translate
#. these to U+00AB (LEFT-POINTING DOUBLE ANGLE
#. QUOTATION MARK), and U+00BB (RIGHT-POINTING DOUBLE ANGLE
#. QUOTATION MARK), respectively.
#.
#. If the catalog has no translation, we will try to
#. use Unicode U+2018 (LEFT SINGLE QUOTATION MARK) and
#. Unicode U+2019 (RIGHT SINGLE QUOTATION MARK).  If the
#. current locale is not Unicode, locale_quoting_style
#. will quote 'like this', and clocale_quoting_style will
#. quote "like this".  You should always include translations
#. for "`" and "'" even if U+2018 and U+2019 are appropriate
#. for your locale.
#.
#. If you don't know what to put here, please see
#. <http://en.wikipedia.org/wiki/Quotation_marks_in_other_languages>
#. and use glyphs suitable for your language.
#: lib/quotearg.c:312
msgid "`"
msgstr "`"

#: lib/quotearg.c:313
msgid "'"
msgstr "'"

#: lib/timevar.c:475
msgid ""
"\n"
"Execution times (seconds)\n"
msgstr ""
"\n"
"Час виконання (у секунда)\n"

#: lib/timevar.c:525
msgid " TOTAL                 :"
msgstr " ЗАГАЛОМ               :"

#: lib/timevar.c:561
#, c-format
msgid "time in %s: %ld.%06ld (%ld%%)\n"
msgstr "час у %s: %ld.%06ld (%ld%%)\n"

#: lib/w32spawn.h:43
#, c-format
msgid "_open_osfhandle failed"
msgstr "Помилка _open_osfhandle"

#: lib/w32spawn.h:84
#, c-format
msgid "cannot restore fd %d: dup2 failed"
msgstr "Не вдалося відновити fd %d: помилка dup2"

#: lib/wait-process.c:223 lib/wait-process.c:255 lib/wait-process.c:317
#, c-format
msgid "%s subprocess"
msgstr "Підпроцес %s"

#: lib/wait-process.c:274 lib/wait-process.c:346
#, c-format
msgid "%s subprocess got fatal signal %d"
msgstr "Підпроцесом %s отримано сигнал щодо аварійного завершення %d"

#~ msgid "stray '@'"
#~ msgstr "зайвий символ '@'"
